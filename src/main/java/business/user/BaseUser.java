package business.user;

class BaseUser implements User {

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    /**
     * Constructor for new user object
     *
     * @param firstName a user's first name
     * @param lastName  a user's last name
     * @param email     a user's email address
     * @param password  a user's password
     */
    public BaseUser(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    /**
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the e-mail
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the e-mail
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
