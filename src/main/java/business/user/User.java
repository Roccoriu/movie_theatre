package business.user;

public interface User {
    /**
     * @return first name
     */
    String getFirstName();

    /**
     * @param firstName first name
     */
    void setFirstName(String firstName);

    /**
     * @return last name
     */
    String getLastName();

    /**
     * @param lastName last name
     */
    void setLastName(String lastName);

    /**
     * @return e-mail
     */
    String getEmail();

    /**
     * @param email e-mail
     */
    void setEmail(String email);

    /**
     * @return password
     */
    String getPassword();

    /**
     * @param password password
     */
    void setPassword(String password);

}
