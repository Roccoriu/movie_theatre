package business.user;

import persistence.user.UserDAO;
import persistence.user.UserDAOFactory;

import java.util.ArrayList;

public class UserFactory {
    /**
     * UserFactory instance
     */
    private static UserFactory instance = null;

    /**
     * UserDAO instance
     */
    private final UserDAO userDAO;

    /**
     * Default constructor
     */
    private UserFactory() {
        UserDAOFactory userDaoFactory = UserDAOFactory.getInstance();
        userDAO = userDaoFactory.getUserDAO();

        insertDefaultUsers();
    }

    /**
     * Method to insert default users so there is always data available
     */
    private void insertDefaultUsers() {
        userDAO.insertUser(new BaseUser("John", "Doe", "john.doe@gmail.com", "test123"));
        userDAO.insertUser(new BaseUser("Jane", "Doe", "jane.doe@gmail.com", "test123"));
        userDAO.insertUser(new BaseUser("Mary", "Smith", "mary.smith@gmail.com", "test123"));
    }


    /**
     * Get or create the UserFactory instance
     *
     * @return UserFactory instance
     */
    public static UserFactory getInstance() {
        if (instance == null) {
            instance = new UserFactory();
        }
        return instance;
    }


    /**
     * Create and save a new user
     *
     * @param firstName a user's first name
     * @param lastName  a user's last name
     * @param email     a user's email address
     * @param password  a user's password
     * @return the newly created user
     */
    public User saveUser(String firstName, String lastName, String email, String password) {
        return userDAO.insertUser(new BaseUser(firstName, lastName, email, password));
    }


    /**
     * @param filter A value to filter the users' first names
     * @return ArrayList of user objects
     */
    public ArrayList<User> getUsers(String filter) {
        return userDAO.getUsers(filter);
    }
}
