package persistence.user;

import business.user.User;

import java.util.ArrayList;

public interface UserDAO {
    /**
     * @param user user to be added
     * @return the created user object
     */
    User insertUser(User user);

    /**
     * @param filter the name to be filtered for
     * @return an ArrayList of user objects
     */
    ArrayList<User> getUsers(String filter);

}
