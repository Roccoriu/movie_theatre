package persistence.user;

public class UserDAOFactory {
    private static UserDAOFactory instance = null;

    /**
     * Default constructor
     */
    private UserDAOFactory() {
    }

    /**
     * @return UserDAOFactory instance
     */
    public static UserDAOFactory getInstance() {
        if (instance == null) {
            instance = new UserDAOFactory();
        }

        return instance;
    }


    /**
     * @return a new UserDAO instance
     */
    public UserDAO getUserDAO() {
        return new UserDAOMock();
    }
}
