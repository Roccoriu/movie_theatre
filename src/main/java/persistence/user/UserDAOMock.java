package persistence.user;

import business.user.User;

import java.util.ArrayList;

class UserDAOMock implements UserDAO {

    /**
     * list of registered users
     */
    private final ArrayList<User> users = new ArrayList<>();

    /**
     * @param user user to be added
     * @return the created user object
     */
    public User insertUser(User user) {
        users.add(user);
        return user;
    }


    /**
     * @param filter the name to be filtered for
     * @return an ArrayList of user objects
     */
    public ArrayList<User> getUsers(String filter) {
        var filtered = users;
        filtered.removeIf(user -> !user.getFirstName().contains(filter));
        return filtered;
    }
}
