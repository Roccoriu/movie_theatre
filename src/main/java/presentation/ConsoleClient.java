package presentation;

import java.util.Scanner;

import business.user.UserFactory;

public class ConsoleClient {
    /**
     * Main method to execute the application
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        var running = true;

        while (running) {
            var selection = printMenu().toUpperCase();

            switch (selection) {
                case "1" -> {
                    registerUser();
                    running = runAgain();
                }
                case "2" -> {
                    getUsers();
                    running = runAgain();
                }
                case "Q" -> running = false;
            }
        }
    }

    /**
     * Prints the menu for the user to choose from
     */
    public static String printMenu() {
        var menuScanner = new Scanner(System.in);

        System.out.print("""
                Welcome to Movie Theatre Tool
                - Create a new user: [1]
                - List all filtered users: [2]
                - Default: Exit [Q]
                """);

        return menuScanner.nextLine();
    }


    /**
     * Asks the user whether to run the action again or to quit
     */
    public static boolean runAgain() {
        var againScanner = new Scanner(System.in);
        System.out.print("Run Again? [y|N]: ");

        return againScanner.nextLine().equalsIgnoreCase("Y");
    }


    /**
     * Register a new user
     */
    public static void registerUser() {
        var scanner = new Scanner(System.in);
        var userFactory = UserFactory.getInstance();

        System.out.print("Enter first name: ");
        var firstName = scanner.nextLine();

        System.out.print("Enter last name: ");
        var lastName = scanner.nextLine();

        System.out.print("Enter e-mail: ");
        var email = scanner.nextLine();

        System.out.print("Enter password: ");
        var password = scanner.nextLine();

        var user = userFactory.saveUser(firstName, lastName, email, password);
        System.out.printf("%s, %s, %s\n", user.getFirstName(), user.getLastName(), user.getEmail());
    }

    /**
     * Print all registered users
     */
    public static void getUsers() {
        var userFactory = UserFactory.getInstance();
        var users = userFactory.getUsers("");


        System.out.print("""
                List of registered users:
                ---------------------------
                """);

        users.forEach(user -> System.out.printf("%s, %s, %s\n", user.getFirstName(), user.getLastName(), user.getEmail()));

        System.out.printf("""
                ---------------------------
                total users: %d
                ---------------------------
                """, users.size());
    }
}