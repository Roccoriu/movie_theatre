package business.user;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class UserFactoryTest {
    private UserFactory userFactory;

    @BeforeEach
    void setUp() {
        userFactory = UserFactory.getInstance();
    }

    @Test
    void getFirstUser() {
        var firstUser = userFactory.getUsers("").get(0);

        assertEquals("John", firstUser.getFirstName(), "First names do not match");
        assertEquals("Doe", firstUser.getLastName(), "Last names do not match");
        assertEquals("john.doe@gmail.com", firstUser.getEmail(), "Emails do");
        assertEquals("test123", firstUser.getPassword(), "Passwords do not match");
    }


    @Test
    void testInsertAndSelectUser() {
        var new_user = userFactory.saveUser("paul", "jefferson", "test@test.com", "12345");
        var saved_user = userFactory.getUsers("paul").get(0);

        assertNotNull(new_user, "User has not been saved!");
        assertNotNull(saved_user, "could not retrieve the user");

        assertEquals(new_user, saved_user, "Users do not match!!");
    }
}